# NOITA **LATEST** BUILD'S DATA CONTENTS
 Repository containing latest Noita publically available build's data.wak contents

`data.wak` file was unpacked using [noita-payer's excellent wakman.exe app](https://github.com/noita-player/noitadocs/releases/latest).

Reference: 
| Release Date                     |          ManifestID |   Branch/Tag   |
| :------------------------------- | :------------------ |  ------------: |
| 23 April 2021 – 14:28:19 UTC     |  [226707737181927261](https://gitlab.com/WUOTE/noita-latest-build-data-contents/-/tree/main/226707737181927261) |   noitabeta    |
